#if !defined(DLL_EXPORT) && !defined(DLL_IMPORT)
#include "FastCGIPlugin.h"
#include <assert.h>

#define TRUE  1
#define FALSE 0

char *WIN_API GetEnvVar(int dwConnID, const char *lpszVarName) {
	if (strcmp(lpszVarName, "SCRIPT_FILENAME") == 0)
		return "C:\\Users\\Venscn\\Desktop\\PLUME_FASTCGI\\Sample\\www\\date.php";
	else if (strcmp(lpszVarName, "SCRIPT_NAME") == 0)
		return "";
	else if (strcmp(lpszVarName, "REQUEST_METHOD") == 0)
		return "GET";

	return "";
}

bool WIN_API Write(int dwConnID, const char *lpvBuffer, size_t length) {
	cout << "-----------------------------------------" << endl;
	cout << "数据长度：" << length << endl;
	if (length > 0) {
		char *buffer = (char *)malloc(length + 1);
		memcpy(buffer, lpvBuffer, length);
		*(buffer + length) = '\0';

		cout << "数据内容：\n" << buffer << endl;
	} else {
		cout << "数据内容：null" << endl;
	}
	cout << "-----------------------------------------" << endl;
	return true;
}

int WIN_API Read(int dwConnID) {
	return NULL;
}

int WIN_API ServerFunc(int dwConnID, int dwAction, int dwFlags, const char *lpvBuffer, size_t length) {
	cout << "-----------------------------------------" << endl;
	cout << "连接标识：" << dwConnID << endl;
	cout << "服务行为：" << dwAction << endl;
	cout << "服务标志：" << dwFlags << endl;
	cout << "数据长度：" << length << endl;
	if (length > NULL) {
		char *buffer = (char *)malloc(length + 1);
		memcpy(buffer, lpvBuffer, length);
		*(buffer + length) = '\0';

		cout << "数据内容：\n" << buffer << endl;
		cout << "-----------------------------------------" << endl;

		if (dwAction == PLUGIN_ACTION_CONFIG) {
			if (strcmp(buffer, "IP") == NULL) {
				return (int)("127.0.0.1");
			} else if (strcmp(buffer, "Port") == NULL) {
				return (int)("9000");
			}
		}

	} else {
		cout << "数据内容：null" << endl;
		cout << "-----------------------------------------" << endl;
	}
	
	return NULL;
}

int main() {
	char *lpszName, *lpszVersion, *lpszAuthor, *lpszDescription;

	// 初始化插件
	int ret = (int)Plume_InitPlugin(MAKEWORD(3, 0), 0);
	assert(ret == TRUE);

	// 获取插件信息
	Plume_GetPluginDesc(&lpszName, &lpszVersion, &lpszAuthor, &lpszDescription);

	cout << "-----------------------------------------" << endl;
	cout << "名称：" << lpszName << endl;
	cout << "版本：" << lpszVersion << endl;
	cout << "作者：" << lpszAuthor << endl;
	cout << "描述：" << lpszDescription << endl;
	cout << "-----------------------------------------" << endl;

	// 插件进程
	ret = Plume_PluginProc(200, GetEnvVar, Write, Read, ServerFunc);
	assert(ret == PLUGIN_OK);

	// 释放插件
	ret = (int)Plume_FreePlugin();
	assert(ret == TRUE);
	
	system("pause");
	return NULL;
}

#endif