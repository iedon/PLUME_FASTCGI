#pragma once

#include "FastCGIParam.h"

#include <winsock.h>
#include <wininet.h>

#pragma comment(lib, "wsock32.lib")

// 版本
#define FCGI_VERSION_1 1

// 头长
#define FCGI_HEADER_LEN  8

// 流程类型
#define FCGI_BEGIN_REQUEST     1
#define FCGI_ABORT_REQUEST     2
#define FCGI_END_REQUEST       3
#define FCGI_PARAMS            4
#define FCGI_STDIN             5
#define FCGI_STDOUT            6
#define FCGI_STDERR            7
#define FCGI_DATA              8
#define FCGI_GET_VALUES        9
#define FCGI_GET_VALUES_RESULT 10
#define FCGI_UNKNOWN_TYPE      11
#define FCGI_MAXTYPE (FCGI_UNKNOWN_TYPE)

// 开始请求角色类型
#define FCGI_RESPONDER  1
#define FCGI_AUTHORIZER 2
#define FCGI_FILTER     3

// 结束请求响应类型
#define FCGI_REQUEST_COMPLETE 0
#define FCGI_CANT_MPX_CONN    1
#define FCGI_OVERLOADED       2
#define FCGI_UNKNOWN_ROLE     3

// 错误类型
#define FCGI_ERROR_OUT 0
#define FCGI_ERROR_LOG 1

// 协议头
typedef struct {
	u_char version;
	u_char type;
	u_char requestIdB1;
	u_char requestIdB0;
	u_char contentLengthB1;
	u_char contentLengthB0;
	u_char paddingLength;
	u_char reserved;
}FCGI_Header;

// 开始请求结构体
typedef struct {
	u_char roleB1;
	u_char roleB0;
	u_char flags;
	u_char reserved[5];
} FCGI_BeginRequestBody;

// 开始请求结构
typedef struct {
	FCGI_Header header;
	FCGI_BeginRequestBody body;
} FCGI_BeginRequestRecord;

// 结束请求结构体
typedef struct {
	u_char appStatusB3;
	u_char appStatusB2;
	u_char appStatusB1;
	u_char appStatusB0;
	u_char protocolStatus;
	u_char reserved[3];
} FCGI_EndRequestBody;

// 错误结构
typedef struct {
	u_char type;
	char *data;
	u_int size;
} FCGI_ERROR;

class FCGI {
public:
	FCGI();
	~FCGI();
	
public:
	u_int SocketID();
	u_int RequestID();

public:
	// 连接服务端
	bool Connect(string ip, u_short port);
	// 发送请求
	bool SendRequest(u_int requestID);
	// 发送参数
	bool SendParam(FCGI_Cache *data);
	// 发送POST
	bool SendStdin(FCGI_Cache *data);
	// 接收回应
	bool RecvResponse(FCGI_Cache *headerData, FCGI_Cache *bodyData);
	// 获取最后错误
	FCGI_ERROR *GetLastError();

private:
	// 构建头部
	FCGI_Header MakeHeader (int type, int requestId, int contentLength, int paddingLength);
	// 构建开始请求体
	FCGI_BeginRequestBody MakeBeginRequestBody (int role, int keepConn);
	// 发送数据
	int SendData(u_int socket, void *data, int length);
	// 接收数据
	int RecvData(u_int socket, void *data, int length);
	// 记录错误
	bool RecordError(bool condition, char *error, u_char type = FCGI_ERROR_OUT);

private:
	FCGI_ERROR error;

protected:
	u_int socketID;
	u_int requestID;
};