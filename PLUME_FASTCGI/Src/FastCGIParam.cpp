#include "FastCGIParam.h"
FCGI_Param::FCGI_Param() {}

FCGI_Param::~FCGI_Param() {}

// 加入参数
void FCGI_Param::Add(string name, string value) {
	if (name.empty()) return;

	int nameSize = name.size();
	int valueSize = value.size();

	// 压入键长
	if (nameSize < 0x80) {
		this->Push (&nameSize, sizeof(u_char));
	} else {
		char _nameSize[4] = {0};

		*_nameSize = (u_char)((nameSize >> 24) | 0x80);
		* (_nameSize + 1) = (u_char)(nameSize >> 16);
		* (_nameSize + 2) = (u_char)(nameSize >> 8);
		* (_nameSize + 3) = (u_char)nameSize;

		this->Push(&_nameSize, sizeof(_nameSize));
	}

	// 压入值长
	if (valueSize < 0x80) {
		this->Push(&valueSize, sizeof(u_char));
	} else {
		char _valueSize[4] = {0};

		*_valueSize = (u_char)((valueSize >> 24) | 0x80);
		* (_valueSize + 1) = (u_char)(valueSize >> 16);
		* (_valueSize + 2) = (u_char)(valueSize >> 8);
		* (_valueSize + 3) = (u_char)valueSize;

		this->Push(&_valueSize, sizeof(_valueSize));
	}
	
	// 压入键值
	this->Push((void *)name.data(), name.size());
	this->Push((void *)value.data(), value.size());
}