#pragma once

#include <iostream>
#include <string>

using namespace std;

// 动态链接库
#ifdef DLL_EXPORT
	#define FCGI_API _declspec(dllexport)
#elif DLL_IMPORT
	#define FCGI_API _declspec(dllimport)
#else
	#define FCGI_API extern
#endif

// 调用约定
#ifdef WIN_API
	#undef WIN_API
	#define WIN_API _stdcall
#else
	#define WIN_API
#endif

// 定义类型
typedef unsigned char   u_char;
typedef unsigned short  u_short;
typedef unsigned int    u_int;
typedef unsigned long   u_long;