#include "FastCGI.h"

FCGI::FCGI() :
socketID(NULL), requestID(NULL)
{
	this->error.data = NULL;
	this->error.size = NULL;
	this->error.type = FCGI_ERROR_OUT;
}

FCGI::~FCGI() {
	if (this->socketID) {
		closesocket(this->socketID);
		WSACleanup();
	}
}

u_int FCGI::SocketID() {
	return this->socketID;
}

u_int FCGI::RequestID() {
	return this->requestID;
}

// 连接服务端
bool FCGI::Connect(string ip, u_short port) {
	WSADATA WSAData;

	// 初始化WSA
	int ret = WSAStartup(MAKEWORD (1, 1), &WSAData);
	if (this->RecordError(ret == NULL, "无法启用 Socket。")) return false;

	// 创建套接字
	this->socketID = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (this->RecordError(this->socketID != INVALID_SOCKET, "初始化 Socket 失败。(INVALID_SOCKET)")) return false;

	sockaddr_in sockaddr;
	sockaddr.sin_family = PF_INET;
	sockaddr.sin_port = htons(port);
	sockaddr.sin_addr.S_un.S_addr = inet_addr(ip.c_str ()); 

	// 连接服务端
	ret = connect(this->socketID, (struct sockaddr *)&sockaddr, sizeof(sockaddr));
	if (this->RecordError(ret != SOCKET_ERROR, "连接 FastCGI 网关失败。")) return false;

	return true;
}

// 发送请求
bool FCGI::SendRequest(u_int requestID) {
	FCGI_BeginRequestRecord beginRecord;
	this->requestID = requestID;
	
	// 构造开始请求数据
	beginRecord.header = this->MakeHeader(FCGI_BEGIN_REQUEST, requestID, sizeof(beginRecord.body), 0);
	beginRecord.body = this->MakeBeginRequestBody(FCGI_RESPONDER, 0);

	// 发送开始请求
	int ret = this->SendData(this->socketID, &beginRecord, sizeof(beginRecord));
	if (this->RecordError(ret == sizeof(beginRecord), "发送 FastCGI 请求记录失败。[ SendRequest () ]")) return false;

	return true;
}

// 发送参数
bool FCGI::SendParam(FCGI_Cache *data) {
	void *paramData = data->Data();
	int paramSize = data->Size();
	int paddingSize = paramSize % 8 ? 8 - (paramSize % 8) : 0;
	
	// 构造参数头
	FCGI_Header header = this->MakeHeader(FCGI_PARAMS, this->requestID, paramSize, paddingSize);

	// 发送参数头
	int ret = this->SendData(this->socketID, &header, FCGI_HEADER_LEN);
	if (this->RecordError(ret == FCGI_HEADER_LEN, "发送 FastCGI 协议头失败。[ SendParam () ]")) return false;

	// 构造参数包
	char paddingData[8] = {0};
	data->Push(paddingData, paddingSize);
	
	// 发送参数包
	ret = this->SendData(this->socketID, paramData, paramSize + paddingSize);
	if (this->RecordError(ret == paramSize + paddingSize, "发送 FastCGI 参数数据失败。[ SendParam () ]")) return false;

	// 构造参数尾
	header = this->MakeHeader (FCGI_PARAMS, this->requestID, 0, 0);

	// 发送参数尾
	ret = this->SendData(this->socketID, &header, FCGI_HEADER_LEN);
	if (this->RecordError(ret == FCGI_HEADER_LEN, "发送 FastCGI 协议尾失败。[ SendParam () ]")) return false;

	return true;
}

// 发送POST
bool FCGI::SendStdin(FCGI_Cache *data) {
	while (!data->Empty()) {
		int localSize = data->Size();
		if (localSize > KB(40))
			localSize = KB(40);
		
		int paddingSize = localSize % 8 ? 8 - (localSize % 8) : 0;

		// 构造数据头
		FCGI_Header header = MakeHeader(FCGI_STDIN, this->requestID, localSize, paddingSize);
		
		// 发送数据头
		int ret = SendData(this->socketID, &header, FCGI_HEADER_LEN);
		if (this->RecordError(ret == FCGI_HEADER_LEN, "发送 FastCGI 协议头失败。[ SendStdin () ]")) return false;

		// 构造数据包
		FCGI_Cache localData;
		data->Pop(&localData, localSize, false);
		if (paddingSize > 0) {
			char paddingData[8] = {0};
			localData.Push(paddingData, paddingSize);
		}

		// 发送数据包
		ret = SendData(this->socketID, localData.Data(), localData.Size());
		if (this->RecordError(ret == localData.Size(), "发送 FastCGI POST数据失败。[ SendStdin () ]")) return false;
	}

	// 构造数据尾
	FCGI_Header header = MakeHeader(FCGI_STDIN, this->requestID, 0, 0);

	// 发送数据尾
	int ret = SendData(this->socketID, &header, FCGI_HEADER_LEN);
	if (this->RecordError(ret == FCGI_HEADER_LEN, "发送 FastCGI 协议尾失败。[ SendStdin () ]")) return false;
	
	return true;
}

// 接收回应
bool FCGI::RecvResponse(FCGI_Cache *headerData, FCGI_Cache *bodyData) {
	FCGI_Header header;
	bool first = true;

	// 接收数据头
	while (RecvData(this->socketID, &header, FCGI_HEADER_LEN) == FCGI_HEADER_LEN) {
		switch (header.type) {

			case FCGI_STDOUT :
			case FCGI_STDERR : {
				int dataSize = (int)(header.contentLengthB1 << 8) + (int)(header.contentLengthB0);
				
				while (dataSize > 0) {
					FCGI_Cache data;

					// 接收临时数据
					void *localData = malloc(dataSize);
					int localSize = RecvData(this->socketID, localData, dataSize);
					
					data.Push(localData, localSize);
					free(localData);
					
					// 寻找数据头
					if (first && header.type == FCGI_STDOUT) {
						char *block = "\r\n\r\n";
						int blockSize = strlen(block);
						int headerEnd = data.Find(block, blockSize);
						
						if (headerEnd > 0) {
							// 完善数据头
							data.Pop(headerData, headerEnd, false);
							first = false;
							
							// 完善数据
							if (localSize != headerEnd + blockSize)
								bodyData->Push((char *)data.Data() + blockSize, data.Size() - blockSize);
						}
					} else {
						// 完善数据
						bodyData->Push((char *)data.Data(), data.Size());
					}

					data.Flush();
					dataSize -= localSize;
				}

				// 接收占位数据
				int paddingLen = (int) header.paddingLength;

				if (paddingLen > 0)
					RecvData(this->socketID, &header, paddingLen);

				// 处理POST错误
				if (header.type == FCGI_STDERR) {
					// 构造错误数据
					int errorSize = bodyData->Size();
					char *error = (char *)malloc(errorSize + 1);
					memcpy(error, bodyData->Data(), errorSize);
					*(error + errorSize) = '\0';

					// 记录错误数据
					if (this->RecordError(false, error, FCGI_ERROR_LOG)) return false;
				}

				continue;}
			case FCGI_END_REQUEST : {
				FCGI_EndRequestBody endBody;

				// 接收结束请求数据
				int ret = RecvData(this->socketID, &endBody, sizeof(FCGI_EndRequestBody));
				if (this->RecordError(ret == sizeof(FCGI_EndRequestBody), "接收 FastCGI 结束记录失败。[ RecvResponse () ]")) return false;

				continue;}
			default : { continue; }
		}
	}

	return true;
}

// 获取最后错误
FCGI_ERROR *FCGI::GetLastError() {
	return &this->error;
}

// 构建头部
FCGI_Header FCGI::MakeHeader (int type, int requestId, int contentLength, int paddingLength) {
	FCGI_Header header;
	header.version = FCGI_VERSION_1;
	header.type = (u_char)type;
	header.requestIdB1 = (u_char)((requestId >> 8) & 0xff);
	header.requestIdB0 = (u_char)(requestId & 0xff);
	header.contentLengthB1 = (u_char)((contentLength >> 8) & 0xff);
	header.contentLengthB0 = (u_char)(contentLength & 0xff);
	header.paddingLength = (u_char)paddingLength;
	header.reserved = 0;
	return header;
}

// 构建开始请求体
FCGI_BeginRequestBody FCGI::MakeBeginRequestBody (int role, int keepConn) {
	FCGI_BeginRequestBody body;
	body.roleB1 = (u_char)((role >> 8) & 0xff);
	body.roleB0 = (u_char)(role & 0xff);
	body.flags  = (u_char)((keepConn) ? 1 : 0);
	memset(body.reserved, 0, sizeof (body.reserved));
	return body;
}

// 发送数据
int FCGI::SendData(u_int socket, void *data, int length) {
	return send (socket, (char *)data, length, 0);
}

// 接收数据
int FCGI::RecvData(u_int socket, void *data, int length) {
	return recv(socket, (char *)data, length, 0);
}

// 记录错误
bool FCGI::RecordError(bool condition, char *error, u_char type) {
	if (!condition) {
		this->error.data = error;
		this->error.size = strlen(error);
		this->error.type = type;
		return true;
	}
	return false;
}