#pragma once

#include "FastCGIConfig.h"

#define KB(__num__) (__num__ * 1024)

// ����
class FCGI_Cache {
public:
	FCGI_Cache();
	FCGI_Cache(const FCGI_Cache &cache);
	virtual ~FCGI_Cache();

public:
	void *Data();
	u_int Size();

	bool Empty();

public:
	// ѹ��
	void Push(void *data, u_int size);
	// ����
	void Pop(void *data, u_int size, bool direction = true);
	void Pop(FCGI_Cache *data, u_int size, bool direction = true);
	// Ѱ��
	u_int Find(void *data, u_int size);
	// ���
	void Flush();

protected:
	void *data;
	u_int size;
};