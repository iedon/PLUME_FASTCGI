#include "FastCGIPlugin.h"

// 获取配置
char *Plume_GetProp(char *prop, int dwConnID,
	int (WIN_API *ServerFunc)(int dwConnID, int dwAction, int dwFlags, const char *lpvBuffer, size_t length))
{
	char *pMain = PLUGIN_NAME;
	char *ret = (char *)ServerFunc(dwConnID, 3,(int)pMain, prop, strlen(prop));
	return ret;
}

// 构造参数
bool Plume_MakeParam(FCGI_Param *paramData, int dwConnID,
	char *(WIN_API *GetEnvVar)(int dwConnID, const char *lpszVarName))
{
	// FCGI 环境变量
	paramData->Add(
		"SCRIPT_FILENAME",
		GetEnvVar(dwConnID, "SCRIPT_FILENAME"));
	paramData->Add (
		"SCRIPT_NAME", 
		GetEnvVar(dwConnID, "SCRIPT_NAME"));
	paramData->Add(
		"REQUEST_METHOD", 
		GetEnvVar(dwConnID, "REQUEST_METHOD"));
	paramData->Add(
		"REQUEST_URI", 
		GetEnvVar(dwConnID, "REQUEST_URI"));
	paramData->Add(
		"QUERY_STRING", 
		GetEnvVar(dwConnID, "QUERY_STRING"));
	paramData->Add(
		"CONTENT_TYPE", 
		GetEnvVar(dwConnID, "CONTENT_TYPE"));
	paramData->Add(
		"CONTENT_LENGTH", 
		GetEnvVar(dwConnID, "CONTENT_LENGTH"));
	paramData->Add(
		"DOCUMENT_URI", 
		GetEnvVar(dwConnID, "DOCUMENT_URI"));
	paramData->Add(
		"DOCUMENT_ROOT", 
		GetEnvVar(dwConnID, "DOCUMENT_ROOT"));
	paramData->Add(
		"GATEWAY_INTERFACE", 
		GetEnvVar(dwConnID, "GATEWAY_INTERFACE"));
	paramData->Add(
		"SERVER_SOFTWARE", 
		GetEnvVar(dwConnID, "SERVER_SOFTWARE"));
	paramData->Add(
		"SERVER_PROTOCOL", 
		GetEnvVar(dwConnID, "SERVER_PROTOCOL"));
	paramData->Add(
		"SERVER_ADDR", 
		GetEnvVar(dwConnID, "SERVER_ADDR"));
	paramData->Add(
		"REMOTE_ADDR", 
		GetEnvVar(dwConnID, "REMOTE_ADDR"));
	paramData->Add(
		"SERVER_PORT", 
		GetEnvVar(dwConnID, "SERVER_PORT"));
	paramData->Add(
		"SERVER_NAME", 
		GetEnvVar(dwConnID, "SERVER_NAME"));
	paramData->Add(
		"REDIRECT_STATUS", 
		GetEnvVar(dwConnID, "REDIRECT_STATUS"));

	// HTTP 环境变量
#ifdef ENV_VAR_HTTP_ALL
	string HTTPAll = string(GetEnvVar(dwConnID, "ALL_HTTP")) + "\r\n";

	int lineStart = 0;
	int lineEnd = HTTPAll.find("\r\n");
	
	while (lineEnd != string::npos) {
		string line = HTTPAll.substr(lineStart, lineEnd);
		HTTPAll = HTTPAll.substr(lineEnd + strlen("\r\n"));
		
		int nameEnd = line.find("=");

		string name = line.substr(lineStart, nameEnd);
		if (!name.empty()) {
			name.erase(0, name.find_first_not_of(" "));
			name.erase(name.find_last_not_of(" ") + 1);
		}

		string value = line.substr(nameEnd + 1, lineEnd);
		if (!value.empty()) {
			value.erase(0, value.find_first_not_of(" "));
			value.erase(value.find_last_not_of(" ") + 1);
		}
		
		paramData->Add(name, value);

		lineEnd = HTTPAll.find ("\r\n");
	}
#else
	paramData->Add(
		"HTTP_ACCEPT", 
		GetEnvVar(dwConnID, "HTTP_ACCEPT"));
	paramData->Add(
		"HTTP_ACCEPT_LANGUAGE", 
		GetEnvVar(dwConnID, "HTTP_ACCEPT_LANGUAGE"));
	paramData->Add(
		"HTTP_ACCEPT_ENCODING", 
		GetEnvVar(dwConnID, "HTTP_ACCEPT_ENCODING"));
	paramData->Add(
		"HTTP_USER_AGENT", 
		GetEnvVar(dwConnID, "HTTP_USER_AGENT"));
	paramData->Add(
		"HTTP_HOST", 
		GetEnvVar(dwConnID, "HTTP_HOST"));
	paramData->Add(
		"HTTP_CONNECTION", 
		GetEnvVar(dwConnID, "HTTP_CONNECTION"));
	paramData->Add(
		"HTTP_CONTENT_TYPE", 
		GetEnvVar(dwConnID, "HTTP_CONTENT_TYPE"));
	paramData->Add(
		"HTTP_CONTENT_LENGTH", 
		GetEnvVar(dwConnID, "HTTP_CONTENT_LENGTH"));
	paramData->Add(
		"HTTP_CACHE_CONTROL", 
		GetEnvVar(dwConnID, "HTTP_CACHE_CONTROL"));
	paramData->Add(
		"HTTP_COOKIE", 
		GetEnvVar(dwConnID, "HTTP_COOKIE"));
	paramData->Add(
		"HTTP_REFERER", 
		GetEnvVar(dwConnID, "HTTP_REFERER"));
#endif
	return true;
}

// 构造POST
bool Plume_MakePost(FCGI_Cache *postData, int dwConnID,
	char *(WIN_API *GetEnvVar)(int dwConnID, const char *lpszVarName),
	int (WIN_API *Read)(int dwConnID))
{
	string contentLenStr = string(GetEnvVar(dwConnID, "CONTENT_LENGTH"));
	if (contentLenStr.empty())
		return true;

	// 获取数据长度
	int contentLen = atoi(contentLenStr.c_str());

	// 读取数据
	if (contentLen < KB(4)) {
		char *contentData = (char *)Read(dwConnID);
		postData->Push(contentData, contentLen);
	} else {
		char *fileName = (char *)Read(dwConnID);
		
		FILE *file = fopen(fileName, "r");
		if (file == NULL)
			return false;

		int readLen = 0;

		while (contentLen > 0) {
			int localLen = contentLen;

			if (localLen > KB(40))
				localLen = KB(40);

			char *contentData = (char *)malloc(localLen);
			fread(contentData, localLen, 1, file);
			fseek(file, readLen, 0);
			readLen += localLen;

			postData->Push(contentData, contentLen);
			free(contentData);

			contentLen -= localLen;
		}

		fclose(file);
	}

	return true;
}

// 构造错误
FCGI_ERROR *Plume_MakeError(FCGI_Cache *errorHeader, FCGI_ERROR *error) {
	// 压入错误头
	char *header = "FastCGI 插件错误：";
	errorHeader->Push(header, strlen(header));

	// 压入错误数据
	errorHeader->Push(error->data, error->size);

	// 重置错误
	error->data = (char *)errorHeader->Data();
	error->size = errorHeader->Size();
	return error;
}

bool Plume_SendToClient(FCGI_Cache *headerData, FCGI_Cache *bodyData, int dwConnID,
	bool (WIN_API *Write)(int dwConnID, const char *lpvBuffer, size_t length),
	int (WIN_API *ServerFunc)(int dwConnID, int dwAction, int dwFlags, const char *lpvBuffer, size_t length))
{
	// 检查头部数据
	char *contentStr = "Content-length";

	if (headerData->Find(contentStr, strlen(contentStr)) == -1) {
		string str = "\r\n" + string(contentStr) + ": ";
		char size[20] = {0};
		itoa (bodyData->Size(), size, 10);

		headerData->Push((void *)str.data(), str.size());
		headerData->Push(size, strlen(size));
	}

	// 发送头部数据
	ServerFunc(dwConnID, PLUGIN_ACTION_HEADER, 0, (const char *)headerData->Data(), headerData->Size());

	// 发送数据内容
	while (!bodyData->Empty()) {
		int localSize = bodyData->Size();
		if (localSize > KB(4))
			localSize = KB(4);

		// 构造临时数据
		FCGI_Cache localData;
		bodyData->Pop(&localData, localSize, false);
		
		// 发送临时数据
		Write(dwConnID, (const char *)localData.Data(), localData.Size());
	}

	// 发送结束命令
	ServerFunc (dwConnID, 5, 0, 0, 0);
	
	return true;
}

// 初始化插件
FCGI_API bool WIN_API Plume_InitPlugin(int serverVersion, int lpszEventIDs) {
	if (MAKEWORD(3, 0) > serverVersion) {
		printf("	Server version not supported.\r\n");
		return false;
	}
	printf("	Plume FastCGI Plugin v1.0\r\n");
	return true;
}

// 释放插件
FCGI_API bool WIN_API Plume_FreePlugin() {
	return true;
}

// 获取插件信息
FCGI_API void WIN_API Plume_GetPluginDesc(char **lpszName, char **lpszVersion, char **lpszAuthor, char **lpszDescription) {
	*lpszName = PLUGIN_NAME;
	*lpszVersion = PLUGIN_VERSION;
	*lpszAuthor = PLUGIN_AUTHOR;
	*lpszDescription = PLUGIN_DESCRIPTION;
}

// 插件进程
FCGI_API int WIN_API Plume_PluginProc (int dwConnID,
	char *(WIN_API *GetEnvVar)(int dwConnID, const char *lpszVarName),
	bool (WIN_API *Write)(int dwConnID, const char *lpvBuffer, size_t length),
	int (WIN_API *Read)(int dwConnID),
	int (WIN_API *ServerFunc)(int dwConnID, int dwAction, int dwFlags, const char *lpvBuffer, size_t length))
{
PLUGIN_RUN(PLUGIN_ENTRY);

PLUGIN_ERROR : 
	// 发送结束命令
	ServerFunc (dwConnID, 5, 0, 0, 0);
	return PLUGIN_FAIL;
	
PLUGIN_ENTRY : 
	FCGI fastCGI;
	FCGI_Cache errorHeader;
	
	// 获取地址端口
	string ip = string(Plume_GetProp("IP", dwConnID, ServerFunc));
	u_short port = atoi(Plume_GetProp("Port", dwConnID, ServerFunc));
	
	// 连接服务端
	if (!fastCGI.Connect(ip, port)) {
		FCGI_ERROR *error = Plume_MakeError(&errorHeader, fastCGI.GetLastError());
		ServerFunc(dwConnID, PLUGIN_ACTION_ERROR, 502, error->data, error->size);
		goto PLUGIN_ERROR;
	}

	// 构造请求标识
	u_int requestID = fastCGI.SocketID();

	// 发送请求
	if (!fastCGI.SendRequest(requestID)) {
		FCGI_ERROR *error = Plume_MakeError(&errorHeader, fastCGI.GetLastError());
		ServerFunc(dwConnID, PLUGIN_ACTION_ERROR, 500, error->data, error->size);
		goto PLUGIN_ERROR;
	}

	// 构造参数数据
	FCGI_Param paramData;
	if (!Plume_MakeParam(&paramData, dwConnID, GetEnvVar)) {
		char *error = "FastCGI 插件错误：请求参数不正确。";
		ServerFunc(dwConnID, PLUGIN_ACTION_ERROR, 500, error, strlen(error));
		goto PLUGIN_ERROR;
	}

	// 发送参数
	if (!fastCGI.SendParam(&paramData)) {
		FCGI_ERROR *error = Plume_MakeError(&errorHeader, fastCGI.GetLastError());
		ServerFunc(dwConnID, PLUGIN_ACTION_ERROR, 500, error->data, error->size);
		goto PLUGIN_ERROR;
	}

	// 构造POST数据
	FCGI_Cache postData;
	if (!Plume_MakePost(&postData, dwConnID, GetEnvVar, Read)) {
		char *error = "FastCGI 插件错误：无法打开上传文件。";
		ServerFunc(dwConnID, PLUGIN_ACTION_ERROR, 500, error, strlen(error));
		goto PLUGIN_ERROR;
	}

	// 发送POST
	if (!fastCGI.SendStdin(&postData)) {
		FCGI_ERROR *error = Plume_MakeError(&errorHeader, fastCGI.GetLastError());
		ServerFunc(dwConnID, PLUGIN_ACTION_ERROR, 500, error->data, error->size);
		goto PLUGIN_ERROR;
	}

	//构造接收数据
	FCGI_Cache headerData, bodyData;

	// 接收回应
	if (!fastCGI.RecvResponse(&headerData, &bodyData)) {
		FCGI_ERROR *error = Plume_MakeError(&errorHeader, fastCGI.GetLastError());
		switch (error->type) {
			case FCGI_ERROR_OUT : {
				ServerFunc(dwConnID, PLUGIN_ACTION_ERROR, 500, error->data, error->size);
			break;}
			case FCGI_ERROR_LOG : {
				ServerFunc(dwConnID, PLUGIN_ACTION_LOG, 0, error->data, error->size);
			break;}
			default : { break; }
		}
		
		goto PLUGIN_ERROR;
	}

	// 发送到客户端
	if (!Plume_SendToClient(&headerData, &bodyData, dwConnID, Write, ServerFunc)) goto PLUGIN_ERROR;

	return PLUGIN_OK;
}