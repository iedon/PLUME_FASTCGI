#pragma once

#include "FastCGI.h"

// 插件状态
#define PLUGIN_OK    0
#define PLUGIN_FAIL -1

// 插件信息
#define PLUGIN_NAME "FastCGI"
#define PLUGIN_VERSION "1.1.2"
#define PLUGIN_AUTHOR "GBD STAR"
#define PLUGIN_DESCRIPTION "为 Plume HTTP Server 提供 FastCGI 支持的插件。"

// 插件行为
#define PLUGIN_ACTION_LOG    1
#define PLUGIN_ACTION_ERROR  2
#define PLUGIN_ACTION_CONFIG 3
#define PLUGIN_ACTION_HEADER 4

// 插件启动
#define PLUGIN_RUN(__entry__) goto __entry__

// 获取配置
char *Plume_GetProp(char *prop, int dwConnID,
	int (WIN_API *ServerFunc)(int dwConnID, int dwAction, int dwFlags, const char *lpvBuffer, size_t length));

// 构造参数
bool Plume_FCGI_MakeParam(FCGI_Param *param, int dwConnID,
	char *(WIN_API *GetEnvVar)(int dwConnID, const char *lpszVarName));

// 构造POST
bool Plume_FCGI_MakePost(FCGI_Cache *postData, int dwConnID,
	char *(WIN_API *GetEnvVar)(int dwConnID, const char *lpszVarName),
	int (WIN_API *Read)(int dwConnID));

// 构造错误
FCGI_ERROR *Plume_MakeError(FCGI_Cache *errorHeader, FCGI_ERROR *error);

// 发送到客户端
bool Plume_SendToClient(FCGI_Cache *header, FCGI_Cache *body, int dwConnID,
	bool (WIN_API *Write)(int dwConnID, const char *lpvBuffer, size_t length),
	int (WIN_API *ServerFunc)(int dwConnID, int dwAction, int dwFlags, const char *lpvBuffer, size_t length));

// 初始化插件
FCGI_API bool WIN_API Plume_InitPlugin(int serverVersion, int lpszEventIDs);

// 释放插件
FCGI_API bool WIN_API Plume_FreePlugin();

// 获取插件信息
FCGI_API void WIN_API Plume_GetPluginDesc(char **lpszName, char **lpszVersion, char **lpszAuthor, char **lpszDescription);

// 插件进程
FCGI_API int WIN_API Plume_PluginProc(int dwConnID,
	char *(WIN_API *GetEnvVar)(int dwConnID, const char *lpszVarName),
	bool (WIN_API *Write)(int dwConnID, const char *lpvBuffer, size_t length),
	int (WIN_API *Read)(int dwConnID),
	int (WIN_API *ServerFunc)(int dwConnID, int dwAction, int dwFlags, const char *lpvBuffer, size_t length));