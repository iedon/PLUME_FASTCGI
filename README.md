# FastCGI For Plume HTTP Server

This is the plugin that designed for Plume HTTP Server to implement the FastCGI Protocol.
This plugin is open source and you can use it under the Apache License.
The source is for study only.

## Platforms

### PC

 - Plume HTTP Server

## Documentation

 - References： [/Doc](https://git.oschina.net/venscn/PLUME_FASTCGI/tree/master/PLUME_FASTCGI/Doc)
 - Examples：[/Sample](https://git.oschina.net/venscn/PLUME_FASTCGI/tree/master/PLUME_FASTCGI/Sample) | 
             [/Bin/Debug](https://git.oschina.net/venscn/PLUME_FASTCGI/tree/master/PLUME_FASTCGI/Bin/Debug)

##License

FastCGI For Plume HTTP Server is available under the terms of the [Apache License] (https://git.oschina.net/venscn/PLUME_FASTCGI/blob/master/LICENSE), please indicate the original author when using.

## 简介

为 Plume HTTP Server 提供 FastCGI 支持的插件。实现 FastCGI 通信协议，可移植为其他服务器插件。

## 支持平台

### PC 端

 - Plume HTTP Server

## 文档与教程

 - 文档目录： [/Doc](https://git.oschina.net/venscn/PLUME_FASTCGI/tree/master/PLUME_FASTCGI/Doc)
 - 示例项目： [/Sample](https://git.oschina.net/venscn/PLUME_FASTCGI/tree/master/PLUME_FASTCGI/Sample) | 
             [/Bin/Debug](https://git.oschina.net/venscn/PLUME_FASTCGI/tree/master/PLUME_FASTCGI/Bin/Debug)

## 开源许可

FastCGI For Plume HTTP Server 遵循 [Apache 协议](https://git.oschina.net/venscn/PLUME_FASTCGI/blob/master/LICENSE)，使用时请标注原作者。